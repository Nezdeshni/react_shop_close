import React from 'react'
import img from '../../img/other/underConst.jpg'
import './404.css'

const UnderDev = () => {
    return (
        <div className='hideBody'>
            <div className='underDev'>
                <img src={img} alt="under development" />
                <h1>Извините !</h1>
                <p>Этот сайт не реагирует на размеры экрана с шириной менее 767px. Мы работаем над этим, и скоро запустим новую версию.</p>
            </div>
        </div>
    )
}

export default UnderDev

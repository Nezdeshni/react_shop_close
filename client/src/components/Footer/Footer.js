import React from 'react'
import { AiFillInstagram, AiFillLinkedin, AiFillGithub } from "react-icons/ai";
import { Link } from 'react-router-dom'
import './Footer.css'

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footerHead'>
        <h1>Подпишитесь на нашу рассылку</h1>
        <p>Получите скидку 15% на первые три заказа</p>
      </div>
      <label className='subscribeLetter' htmlFor="subscribe">
        <input name='subscribe' type="text" placeholder='Ваш email' />
        <button>Подписаться</button>
      </label>
      <section className='footerFoot'>
        <ul className='checkout'>
          <li>
            <Link to={'./blog'}>
              Блог
            </Link>
          </li>
          <li>
            <Link to={'./shop'}>
              Магазин
            </Link>
          </li>
          <li>
            <Link to={'./about'}>
              О нас
            </Link>
          </li>
          <li>
            <Link to={'./contact'}>
              Контакты
            </Link>
          </li>
        </ul>
        <span className='credits'>Разработано в рамках ВКР</span>
        <ul className='socialHandels'>
          <li>
            <a href="https://www.instagram.com/ig_matin" target="_blank" rel="noreferrer">
              <AiFillInstagram />
            </a>
          </li>
          <li>
            <a href="https://www.linkedin.com/in/manish-kumar-09a114184" target="_blank" rel="noreferrer">
              <AiFillLinkedin />
            </a>
          </li>
          <li>
            <a href="https://github.com/inovatormatin" target="_blank" rel="noreferrer">
              <AiFillGithub />
            </a>
          </li>
        </ul>
      </section>
    </footer>
  )
}

export default Footer;

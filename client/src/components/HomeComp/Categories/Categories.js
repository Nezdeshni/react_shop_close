import React from "react";
import { useNavigate } from 'react-router-dom';
import "./Categories.css";

const Categories = () => {
  const navigate = useNavigate()
  const myData = [
    {
      image:
        "https://res.cloudinary.com/inovatormatin/image/upload/v1653646901/eway/category/category1_cyau86.jpg",
      title: "Женская коллекция",
      alt: "category1",
    },
    {
      image:
        "https://res.cloudinary.com/inovatormatin/image/upload/v1653646901/eway/category/category2_u3iqzo.jpg",
      title: "Мужская коллекция",
      alt: "category2",
    },
    {
      image:
        "https://res.cloudinary.com/inovatormatin/image/upload/v1653646901/eway/category/category3_mu1u1m.jpg",
      title: "Аксесуары",
      alt: "category3",
    },
  ];
  const clickhandler = (value) => {
    navigate('/shop', {
      state: {
        category: value
      }
    })
  }
  return (
    <section className="categories">
      {myData.map((card, index) => (
        <div className="Card" key={index} onClick={() => clickhandler(card.title)}>
          <img className="CardImg" src={card.image} alt={card.alt} />
          <div className="Cardtext">
            <h3>{card.title}</h3>
            <button>К покупкам</button>
          </div>
        </div>
      ))}
    </section>
  );
};

export default Categories;

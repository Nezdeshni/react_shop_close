import React from 'react'
import './Newproduct.css'
import { Product, RingLoader } from '../../index'
import { useSelector } from 'react-redux'

const Newproduct = () => {
  const productState = useSelector(state => state.getallProducts);
  const { products } = productState;
  return (
    <div className='newproduct'>
        <section className='npHeading'>
            <span>Наш магазин</span>
            <h3>Наша продукция</h3>
            <p>Просмотрите нашу новую коллекцию в соответствии с категорией лучших интересных продуктов.</p>
        </section>
        <section className='npgallery'>
            {products !== null ?
            products.slice(8, 12).map(((product, index) =>
            <Product key={index} product={product}/>
            ))
            : ''}
        </section>
        {products === null ? <RingLoader /> : ''}
    </div>
  )
}

export default Newproduct;

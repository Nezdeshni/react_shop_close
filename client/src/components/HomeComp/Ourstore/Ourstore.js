import React from 'react'
import './Ourstore.css'
import { Product, RingLoader } from '../../index'
import { useSelector } from 'react-redux'

const Ourstore = () => {
  const productState = useSelector(state => state.getallProducts);
  const { products } = productState;
  return (
    <div className='ourStore'>
        <section className='storeHeading'>
            <span>Наш магазин</span>
            <h3>ГОРЯЧИЕ ПОКУПКИ СЕЗОНА</h3>
            <p>Вы обязательно найдете то, что ищете. Просмотрите нашу коллекцию в соответствии с категорией лучших интересных товаров.</p>
        </section>
        <section className='storegallery'>
        {products !== null ?
          products.slice(0, 8).map(((product, index) =>
          <Product key={index} product={product}/>
          ))
          : '' }
        </section>
        {products === null ? <RingLoader /> : ''}
    </div>
  )
}

export default Ourstore;

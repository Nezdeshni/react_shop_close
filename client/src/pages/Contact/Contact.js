import React, { useState, useEffect } from "react";
import "./Contact.css";
import { useSelector, useDispatch } from "react-redux";
import { SpinLoader } from "../../components"
import { sendMessageContact } from "../../actions/utilsAction";

const Contact = () => {
  let userName = localStorage.getItem("userName");
  let userEmail = localStorage.getItem("userEmail");
  const [userMessage, setUserMessage] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
  });
  const dispatch = useDispatch();
  const contactState = useSelector(state => state.sendMessage);

  // on input set values
  const onChangeHandler = (e) => {
    let obj = {
      ...userMessage,
      [e.target.name]: e.target.value
    }
    setUserMessage(obj)
  };

  // on form submit
  const submitHandler = (e) => {
    e.preventDefault();
    if (userName !== null) {
      let msg = {
        ...userMessage,
        name: userName,
        email: userEmail
      }
      alert("up")
      console.log(msg)
      dispatch(sendMessageContact(msg));
    } else {
      alert("low")
      dispatch(sendMessageContact(userMessage));
    }
    let obj = {
      ...userMessage,
      subject: "",
      message: ""
    }
    setUserMessage(obj)
  };

  // to scroll on top automatically
  useEffect(() => {
    window.scrollTo(0, 0)
  })

  return (
<div className="contact">
  <div className="contactTitle">
    <h2>Контакты</h2>
  </div>
  <div className="contactContain">
    <form onSubmit={(e) => submitHandler(e)} method='post'>
      <input
        type="text"
        value={userName === null ? userMessage.name : userName}
        disabled={userName === null ? false : true}
        onChange={(e) => onChangeHandler(e)}
        name="name"
        required
        placeholder="Ваше имя*"
      />
      <input
        type="text"
        value={userEmail === null ? userMessage.email : userEmail}
        disabled={userEmail === null ? false : true}
        onChange={(e) => onChangeHandler(e)}
        name="email"
        required
        placeholder="Ваш email*"
      />
      <input
        type="text"
        value={userMessage.subject}
        onChange={(e) => onChangeHandler(e)}
        name="subject"
        required
        placeholder="Тема*"
      />
      <textarea
        value={userMessage.message}
        onChange={(e) => onChangeHandler(e)}
        name="message"
        required
        cols="30"
        rows="5"
        placeholder="Ваше сообщение*"
      />
      {
        <button type="submit" disabled={contactState.loading}>
          {contactState.loading ?
            <SpinLoader /> :
            'Отправить'
          }
        </button>
      }
    </form>
    <iframe
      title="title"
      src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1260.0922480833863!2d37.62155166387077!3d55.75452301353187!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a598b4552fd%3A0xd2f2265b70fe6a05!2sGUM!5e0!3m2!1sen!2sru!4v1712849677256!5m2!1sen!2sru"
      loading="lazy"
      referrerpolicy="no-referrer-when-downgrade"
      className="map"
    />
  </div>
</div>

  );
};

export default Contact;
